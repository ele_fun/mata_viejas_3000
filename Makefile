BIN_API_REST=bin/api_rest

all:clean build scp

build:
	#go build -o $(BIN_API_REST) cmd/api-rest/*.go
	env GOARCH=arm64 GOOS=linux go build -o $(BIN_API_REST) cmd/api-rest/*.go

#a_run: $(BIN_API_REST)
	#$(BIN_API_REST)

clean:
	rm bin/*

scp: build
	scp $(BIN_API_REST) r@192.168.18.30:/home/r/Documents
	#scp .env r@192.168.18.30:/home/r/Documents

