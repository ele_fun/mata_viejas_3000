package dispatch

type (
	dispatcher interface {
		add(f func())
		run()
	}
	dispatch struct {
		listener chan func()
	}
)

var d dispatcher

func Init() {
	d = &dispatch{listener: make(chan func(), 10)}
}

// Apply repository pattern
func Add(f func()) {
	d.add(f)
}

func Run() {
	d.run()
}

// implementation dispatcher

func (d *dispatch) add(f func()) {
	d.listener <- f
}

func (d *dispatch) run() {
	for {
		select {
		case f := <-d.listener:
			go f()
		}
	}
}
