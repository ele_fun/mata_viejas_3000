package raspberry

import (
	"log"
	"sync"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
)

type (
	servoMotor struct{}
)

var (
	lock       *sync.Mutex = &sync.Mutex{}
	ServoMotor             = servoMotor{}
)

func (s *servoMotor) Angle(angle int) {

	if err := rpio.Open(); err != nil {
		log.Fatalln("init raspberry, error: ", err)
	}

	log.Println("serMotor Up")
	pinProof := rpio.Pin(18) // GPIO 18
	pinProof.Pwm()
	pinProof.Freq(64000)
	cycle := 1280
	// 50Hz -> 64000 / 1280

	pinProof.DutyCycle(setAngle(angle), uint32(cycle))
}

func (s *servoMotor) Up() {

	if err := rpio.Open(); err != nil {
		log.Fatalln("init raspberry, error: ", err)
	}

	log.Println("serMotor Up")
	pinProof := rpio.Pin(18) // GPIO 18
	pinProof.Pwm()
	pinProof.Freq(64000)
	cycle := 1280
	// 50Hz -> 64000 / 1280
	pinProof.DutyCycle(0, uint32(cycle))
	sleep := 200

	pinProof.DutyCycle(dLen(0, cycle), uint32(cycle))
	time.Sleep(time.Duration(sleep) * time.Millisecond)
	lock.Lock()
	for index := (0); index < 100; index += 5 {
		log.Println("index: ", index)

		pinProof.DutyCycle(dLen(0, cycle), uint32(cycle))
		time.Sleep(time.Duration(sleep) * time.Millisecond)

		pinProof.DutyCycle(dLen(90, cycle), uint32(cycle))
		time.Sleep(time.Duration(sleep) * time.Millisecond)

		pinProof.DutyCycle(dLen(180, cycle), uint32(cycle))
		time.Sleep(time.Duration(sleep) * time.Millisecond)

		pinProof.DutyCycle(dLen(90, cycle), uint32(cycle))
		time.Sleep(time.Duration(sleep) * time.Millisecond)

		pinProof.DutyCycle(dLen(0, cycle), uint32(cycle))
		time.Sleep(time.Duration(sleep) * time.Millisecond)
	}

	lock.Unlock()
}
func (s *servoMotor) Toggle() {

	if err := rpio.Open(); err != nil {
		log.Fatalln("init raspberry, error: ", err)
	}

	pin := rpio.Pin(12)
	pin.Output()
	time.Sleep(time.Second)
	pin.Low()

	// Toggle pin 20 times
	for x := 0; x < 5; x++ {
		pin.High()
		time.Sleep(time.Second)
		pin.Low()
		time.Sleep(time.Second)
	}
	time.Sleep(time.Second)
	pin.Low()
}
func (s *servoMotor) Brightness() {

	if err := rpio.Open(); err != nil {
		log.Fatalln("init raspberry, error: ", err)
	}

	pin := rpio.Pin(19)
	pin.Mode(rpio.Pwm)
	pin.Freq(64000)
	pin.DutyCycle(0, 32)
	// the LED will be blinking at 2000Hz
	// (source frequency divided by cycle length => 64000/32 = 2000)

	// five times smoothly fade in and out
	for i := 0; i < 5; i++ {
		for i := uint32(0); i < 32; i++ { // increasing brightness
			pin.DutyCycle(i, 32)
			time.Sleep(time.Second / 32)
		}
		for i := uint32(32); i > 0; i-- { // decreasing brightness
			pin.DutyCycle(i, 32)
			time.Sleep(time.Second / 32)
		}
	}
}

// UpWithOutFreq - por alguna razon, esto método no mueve el servo motor
// me parece que se debe a la frecuencia debe ser obtenida de acuerdo aun cálculo
func (s *servoMotor) UpWithOutFreq() {

	if err := rpio.Open(); err != nil {
		log.Fatalln("init raspberry, error: ", err)
	}

	log.Println("serMotor Up")
	pinProof := rpio.Pin(18)
	pinProof.Pwm()
	cycle := uint32(50)
	pinProof.DutyCycle(0, uint32(cycle))

	for index := uint32(0); index < 5; index++ {
		log.Println("index: ", index)
		pinProof.DutyCycle(0, uint32(cycle))
		time.Sleep(1000 * time.Millisecond)

		pinProof.DutyCycle(1, uint32(cycle))
		time.Sleep(1000 * time.Millisecond)

		pinProof.DutyCycle(2, uint32(cycle))
		time.Sleep(1000 * time.Millisecond)

		pinProof.DutyCycle(1, uint32(cycle))
		time.Sleep(1000 * time.Millisecond)
	}
}

func setAngle2To10Percent(angle, cycle int) (duty uint32) {
	bla := (float64(angle) / 1800.0) + float64(2.00/100.00)
	return (uint32(cycle) * uint32(bla))
}
func setAngle(angle int) (duty uint32) {
	//bla := 16.00 * float64(angle) / 45.00
	//bla := 16.00 * float64(angle) / 90.00
	//bla := 16.00 * float64(angle) / 27.00
	bla := 16.00 * float64(angle) / 22.00 // se utiliza 22, por la precisión del servomotor
	return 1 * (64 + uint32(bla))
}

func dLen(angle, cycle int) uint32 {
	//return uint32((angle * cycle) / 1500)
	bla := ((180.00 - float64(angle)) * 16.00) / 45.00
	return 128 - uint32(bla)
}

func duttyLen(angle int, cycle int) (d uint32) {
	return setAngle(angle) * uint32(cycle) / 100
}
