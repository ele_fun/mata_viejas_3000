package raspberry

import (
	"log"

	"github.com/stianeikeland/go-rpio/v4"
)

var (
	// PINES A USAR PARA EL RASPBERRY
	pinServoMotor = rpio.Pin(12)
)

func init() {
	if err := rpio.Open(); err != nil {
		log.Fatalln("init raspberry, error: ", err)
	}
	//defer rpio.Close()

	log.Println("init raspberry pi")

	//pinServoMotor.Output()
	//pinServoMotor.Pwm()
	pinServoMotor.Mode(rpio.Pwm)
	pinServoMotor.Freq(50)
	//pinServoMotor.DutyCycle(0, 50)
}
