package config

import (
	"log"

	"github.com/ilyakaznacheev/cleanenv"
)

type (
	env struct {
		Port string `env:"port" env-default:"9091"`
	}
)

var (
	Env env
)

func init() {
	err := cleanenv.ReadConfig(".env", &Env)
	if err != nil {
		log.Fatalln("init .env, error: ", err)
	}
}
