package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/focazo/internal/handler/servo"
)

func servoRouter() []router {

	return []router{
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/servo/run"),
			Handler: servo.Run,
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/servo/proof"),
			Handler: servo.Proof,
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/servo/toggle"),
			Handler: servo.Toggle,
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/servo/b"),
			Handler: servo.Brightness,
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/servo/up"),
			Handler: servo.Up,
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/servo/angle"),
			Handler: servo.Angle,
		},
	}
}
