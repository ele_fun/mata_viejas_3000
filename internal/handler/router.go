package handler

import (
	"net/http"
)

type (
	router struct {
		Method  string
		Path    string
		Handler http.HandlerFunc
	}
)

var Routers []router = []router{}

func init() {
	Routers = append(Routers, servoRouter()...)
}
