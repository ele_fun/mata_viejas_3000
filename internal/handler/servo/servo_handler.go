package servo

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/Elevarup/ele_fun/focazo/internal/services/dispatch"
	"gitlab.com/Elevarup/ele_fun/focazo/internal/services/raspberry"
)

func Run(w http.ResponseWriter, r *http.Request) {
	timeValue := r.URL.Query().Get("time")

	// DONE: convert timeValue string to int
	t, err := strconv.Atoi(timeValue)
	if err != nil {
		t = 5
	}
	dispatch.Add(func() {
		log.Println("init, time: ", t, " s")
		now := time.Now()
		time.Sleep(time.Second * time.Duration(t))
		log.Println("Since: ", time.Since(now).Seconds())
	})
}
func Proof(w http.ResponseWriter, r *http.Request) {

	dispatch.Add(func() {
		now := time.Now()
		log.Println("run servo, handler proof")
		raspberry.ServoMotor.Up()
		log.Println("Since: ", time.Since(now).Seconds())
	})

}
func Toggle(w http.ResponseWriter, r *http.Request) {

	dispatch.Add(func() {
		now := time.Now()
		log.Println("run servo, handler toggle")
		raspberry.ServoMotor.Toggle()
		log.Println("Since: ", time.Since(now).Seconds())
	})

}
func Brightness(w http.ResponseWriter, r *http.Request) {

	dispatch.Add(func() {
		now := time.Now()
		log.Println("run servo, handler Brightness")
		raspberry.ServoMotor.Brightness()
		log.Println("Since: ", time.Since(now).Seconds())
	})

}
func Up(w http.ResponseWriter, r *http.Request) {

	dispatch.Add(func() {
		now := time.Now()
		log.Println("run servo, handler proof")
		raspberry.ServoMotor.UpWithOutFreq()
		log.Println("Since: ", time.Since(now).Seconds())
	})

}
func Angle(w http.ResponseWriter, r *http.Request) {
	var angle int
	dataAngle := r.URL.Query().Get("angle")
	if dataAngle != "" {
		angle, _ = strconv.Atoi(dataAngle)
	}

	dispatch.Add(func() {
		now := time.Now()
		log.Println("run servo, handler Angle")
		raspberry.ServoMotor.Angle(angle)
		log.Println("Since: ", time.Since(now).Seconds())
	})

}
