package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/Elevarup/ele_fun/focazo/internal/config"
	"gitlab.com/Elevarup/ele_fun/focazo/internal/handler"
	"gitlab.com/Elevarup/ele_fun/focazo/internal/services/dispatch"
)

func main() {
	// INICIALIZACIÓN DEL DISPATCH
	dispatch.Init()
	go dispatch.Run()

	log.Println("Server run, port: ", config.Env.Port)
	// inicializar el servidor
	srv := http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%s", config.Env.Port),
		Handler: serverMux(),
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalln("error, listen and serve: ", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	srv.Shutdown(ctx)
	log.Println("Server shutdown")
}

func serverMux() *mux.Router {
	r := mux.NewRouter()

	for _, route := range handler.Routers {
		log.Println("Path: ", route.Path, " - method: ", route.Method)
		r.HandleFunc(route.Path, route.Handler).Methods(route.Method)
	}

	return r
}
